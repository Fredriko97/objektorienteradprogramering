package activity;

import java.util.*;

import data.DataRead;
import database.*;
import trackPoint.*;

public class Activity {

	
	private int activityId;
	private String activityName;
	private int userId;
	private int typeId;
	private List<TrackPoint> pointList;
	
	public Activity(int activityId, String activityName, int userId, int typeId) {
		this.activityId = activityId;
		this.activityName = activityName;
		this.userId = userId;
		this.typeId = typeId;
	}
	
	public int getTypeId() {
		return typeId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getActivityId() {
		return this.activityId;
	}
	
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	

	public List<TrackPoint> getPointList() {
		return this.pointList; 
	}
	
	
	public void setPointList() {
        PointDAO pointsDao = new PointDAO();
        this.pointList = pointsDao.getActivityPointsById(this.activityId);
    }
	
	
	public String getName() {
		return this.activityName;
	}

}
