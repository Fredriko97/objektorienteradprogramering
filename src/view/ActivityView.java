package view;

import java.awt.Dimension;
import java.awt.geom.Point2D;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import controller.Controller;

public class ActivityView {
	
	private Controller cont = new Controller();
	private int activityId;
	String [] stats; 
	
	public ActivityView() {
		//this.activityId = activityId;
			
	}
	
	public JTabbedPane activityPane(int activityId) {
		
		JFrame activityFrame = new JFrame("Aktiviteter ");
		JTabbedPane tabbedPane = new JTabbedPane(); 
		
		tabbedPane.addTab("Information" , generalActivity(activityId));
		tabbedPane.addTab("Puls �ver tid", activityStats(activityId));
		tabbedPane.addTab("Hastighet", speedActiviy(activityId));
		tabbedPane.addTab("Altitud", altitudeActivity(activityId));
		tabbedPane.addTab("Karta ", mapActivity(activityId));
		tabbedPane.addTab("�ndra info", changeInfo(activityId));
		tabbedPane.revalidate();
		tabbedPane.repaint();
		
		 
		 activityFrame.setVisible(true);
		 activityFrame.add(tabbedPane);
		 activityFrame.setSize(800, 600);
		 activityFrame.revalidate();
		 activityFrame.repaint();
			
		return tabbedPane ;
	}

	public JPanel activityStats(int activityId) {
		
		JPanel panel = new JPanel();
	
		panel.setPreferredSize(new Dimension(800, 600));
		List<List> tempList = cont.getGraphData(activityId, 0);
		panel.setVisible(true);		
		 panel = new PlotView("Puls �ver tid ", tempList.get(0), tempList.get(1));				 
		 panel.setVisible(true);
		 panel.setSize(800, 600);
		 panel.revalidate();
		 panel.repaint();
		return panel;
	}
	
	public JPanel generalActivity(int activityId) {
		JPanel panel = new JPanel();
		JTable jt = new JTable(19, 2); 
		jt.setSize(800,600);
		String [] labels = {"Start  ","Stopp ","Tidsf�rlopp ", "Distans", "Minsta puls", "Maxpuls ","Medelpuls ","Minsta hastighet ","Max hastighet ","Medel hastighet ","Min Alt ","Max altitud ","Medel altitud ","Minsta lutning ","Max lutning ", "Medel lutning ","Min Cad ","Max Cad ","Medel Cad "};
		stats = cont.getStats(activityId);
		for(int i = 0; i < stats.length; i++) {
			jt.setValueAt(stats[i], i, 1);
			jt.setValueAt(labels[i], i, 0); 
			
		}
			
		panel.add(jt);

		return panel;
	}
	
	public JPanel speedActiviy(int activityId) {
		JPanel panel = new JPanel();
		List<List> tempList = cont.getSpeedData(activityId, 0);
		panel = new PlotView("Hastighet ", tempList.get(0), tempList.get(1));
		panel.setVisible(true);
		panel.setSize(800, 600);
		panel.revalidate();
		panel.repaint();
		
		
		
		return panel; 
	}
	

	
	public JPanel altitudeActivity(int activityId) {
		JPanel panel = new JPanel();
		List<List> tempList = cont.getAltitudeData(activityId, 0);
		panel = new PlotView("Hastighet ", tempList.get(0), tempList.get(1));
		panel.setVisible(true);
		panel.setSize(800, 600);
		panel.revalidate();
		panel.repaint();
		
		return panel;
	}
	
	public JPanel changeInfo(int activityId) {
		JPanel panel = new JPanel();
		JTextField newName = new JTextField("Skriv in det nya namnet h�r");
		JButton setName = new JButton("Byt namn");
		setName.addActionListener(e->{
			System.out.println("Byter namn");
			cont.updateName(newName.getText(), activityId);
			panel.revalidate();
			panel.repaint();
		});
		
		panel.add(newName);
		panel.add(setName);
		panel.setVisible(true);
		panel.setSize(800, 600);
		panel.revalidate();
		panel.repaint();
		
		return panel;
	}
	
	public JPanel mapActivity(int activityId) {
		JPanel panel = new JPanel();
		
		PlotMap plotMap = new PlotMap(activityId);
	
		
		panel.add(plotMap);
		panel.setVisible(true);
		panel.setSize(800, 600);
		panel.revalidate();
		panel.repaint();
		
		
		return panel;
	}



	
	
	
}
