package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import activity.Activity;
import database.PointDAO;
import trackPoint.TrackPoint;

public class PlotMap extends JPanel {

	List<TrackPoint> list;

	//private double latDiff; 
	//private double longDiff;
	private double minLon;
	private double maxLon;
	private double minLat;
	private double maxLat;
	private PointDAO pointDao = new PointDAO();
	int[] xArray;
	int[] yArray;

	private int activityId;
	
	public PlotMap (int activityId) {
		this.activityId = activityId;
		list = pointDao.getActivityPointsById(activityId);
		this.setPreferredSize(new Dimension(800, 600));
		
	}
	public void paintComponent(Graphics g) {
		
		super.paintComponent(g);
	
		findMaxMinLongLat();
		
		
		xArray= new int[list.size()];
		yArray= new int[list.size()];
		
		int i = 0;
		for(TrackPoint p : list) {
			
			xArray[i] = getXPixValue(p);
			yArray[i] = getYPixValue(p);
			i++;
		}		
		g.setColor(Color.BLUE);
		g.drawPolyline(xArray,yArray, xArray.length);
		

	}
	
public  void findMaxMinLongLat() {
		
		minLat = list.get(0).getLatitude();
		minLon = list.get(0).getLongitude();
		maxLat = list.get(0).getLatitude();
		maxLon = list.get(0).getLongitude();
		
		for(TrackPoint tp : list) {
			
			double lon = tp.getLongitude();
			double lat = tp.getLatitude();
			
			if(lon > maxLon)
				maxLon = lon; 
			else if(lon < minLon)
					minLon = lon; 
			if(lat > maxLat)
					maxLat = lat; 
			else if( lat < minLat)
					minLat = lat;
			
		}
		
	

	}
	
	private int getXPixValue(TrackPoint tp) {
		int width = this.getWidth();
		int xPix = (int)(((maxLon - tp.getLongitude()) / (maxLon -minLon)) * width);
		return xPix;
		
	}
	private int getYPixValue(TrackPoint tp) {
		int height = this.getHeight();
		int yPix = (int)(((maxLat - tp.getLatitude()) / (maxLat - minLat)) * height);
		//yPix = getHeight()-yPix;
		return yPix;
		
	}

}
