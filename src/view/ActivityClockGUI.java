package view;

import java.awt.*;
import java.awt.event.ActionListener;
import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import javax.swing.*;
import javax.swing.filechooser.FileSystemView;

import org.postgresql.util.PSQLException;

import activity.Activity;
import controller.Controller;
import database.DaoType;
import database.PointDAO;
import trackPoint.TrackPoint;
import user.User;

public class ActivityClockGUI extends JFrame {
	Controller cont = new Controller();
	private int signedInUser;
	String filePath; 
	ActivityView av;
	
	public ActivityClockGUI() {
	this.av = new ActivityView();
	login(this);
	}
	
	public void startMainFrame(String userName) {
		JFrame frame = new JFrame(userName);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(300,70);
		frame.setBackground(Color.white);
		frame.setVisible(true);
		frame.add(HeadPanel());
	}
	
	
	@SuppressWarnings("deprecation")
	public void login(JFrame frame) {
		
			
			JPanel panel = new JPanel(new BorderLayout(5, 5));

			JPanel label = new JPanel(new GridLayout(0, 1, 2, 2));
			label.add(new JLabel("Anv�ndarnamn ", SwingConstants.RIGHT));
			label.add(new JLabel("L�senord ", SwingConstants.RIGHT));
			panel.add(label, BorderLayout.WEST);

			JPanel controls = new JPanel(new GridLayout(0, 1, 2, 2));
			JTextField username = new JTextField();
			controls.add(username);
			JPasswordField password = new JPasswordField();
			controls.add(password);
			panel.add(controls, BorderLayout.CENTER);

			JOptionPane.showMessageDialog(frame, panel, "Logga in", JOptionPane.QUESTION_MESSAGE);
			
			
			System.out.println(username.getText());
			System.out.println(password.getPassword());
			
		
			try {
				if(cont.login(username.getText(), password.getText())) {
					startMainFrame(username.getText());
					signedInUser = cont.getUserId(username.getText());
				}else {
					
					login(frame);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	    
	}

	private JPanel HeadPanel() {

		JPanel headPanel = new JPanel(new BorderLayout());
		headPanel.add(NorthPanel(headPanel), BorderLayout.NORTH);
		headPanel.add(SouthPanel(headPanel), BorderLayout.SOUTH);
		headPanel.setBackground(Color.white);
		return headPanel;
	}

	private JPanel NorthPanel(JPanel headPanel) {

		JPanel northPanel = new JPanel(new GridLayout(1, 3));
		JButton northButton1 = new JButton("Anv�ndarinformation ");
		JButton northButton2 = new JButton("Dina aktiviteter ");
		northPanel.add(northButton2);
		northPanel.add(northButton1);
		northButton2.addActionListener(e -> {
			activitys();
			//av.activityStats(1);
		});
		northButton1.addActionListener(e -> {
			UserInfo();
		});
	
		return northPanel;

	}


	public JFrame activitys() {
		
	ArrayList<Activity> list = cont.getActivity();
		
	JComboBox<String> activityList = new JComboBox<String>();
	
		for(int i = 0; i < list.size(); i++) {
			activityList.addItem(list.get(i).getName());
		}

		
		JButton selectActivity = new JButton("V�lj ");
		selectActivity.addActionListener(e -> {
			//av.activityStats(list.get(activityList.getSelectedIndex()).getActivityId())
			
			av.activityPane(list.get(activityList.getSelectedIndex()).getActivityId());
			;});

		JButton createActivity = new JButton("Skapa aktivitet"); 
		createActivity.addActionListener(e-> { 
			createActivityFrame();
		});
		
		JFrame frameActive = new JFrame("Aktiviteter");
		JPanel activity = new JPanel(); 
		frameActive.setSize(400,300);
		frameActive.setBackground(Color.black);
		frameActive.setVisible(true);
		frameActive.add(activity);
		activity.add(activityList);
		activity.add(selectActivity);
		activity.add(createActivity);		
		return frameActive;
	}

	
	private JPanel SouthPanel(JPanel headPanel) {

		JPanel southPanel = new JPanel(new GridLayout(3, 1));
		return southPanel;

	}
	
	
private JFrame UserInfo() {

	
		JTable jt = new JTable(2, 2); 
		ArrayList infoList = cont.getUserInfo2();
		String [] labels = {"�lder", "Vikt"};
		for(int i = 0; i < infoList.size(); i++) {
			jt.setValueAt(infoList.get(i), i, 1);
			jt.setValueAt(labels[i], i, 0); 
			
		}
	
		String [] infoString = cont.getUserInfo(signedInUser) ;
	
		JTextField ageField = new JTextField("�lder");
		JTextField weightField = new JTextField("Vikt");
		JButton setInfo = new  JButton("St�ll in");
		setInfo.addActionListener(e->{
			cont.updateUserInfo(Integer.parseInt(ageField.getText()), Integer.parseInt(weightField.getText()));
		});
		
		
		JFrame info = new JFrame(infoString[1] + "s Info");
		JPanel infoPanel = new JPanel();
		info.setSize(400, 300);
		info.setBackground(Color.black);
		info.setVisible(true);
		jt.setVisible(true);
		info.add(infoPanel);
		infoPanel.add(jt);
		infoPanel.add(ageField);
		infoPanel.add(weightField);
		infoPanel.add(setInfo);
		
		return info; 
	} 


public JFrame createActivityFrame(){
	
	JFrame activityFrame = new JFrame("Skapa aktivitet : ");
	activityFrame.setSize(400,300);
	activityFrame.setVisible(true);
	JPanel panel = new JPanel();
	activityFrame.add(panel);
	panel.setVisible(true);
	JTextField nameField = new JTextField("Skriv aktivitetsnamn: "); 
	String[] activityTypes = {"L�pning", "Cyckling", "Simning", "�vrigt"};
	JComboBox<String> activityTypeList = new JComboBox<String>(activityTypes);
	
	JButton b1 = new JButton("Bl�ddra : "); 
	JButton b2 = new JButton("Skapa: "); 
	panel.add(b1);
	panel.add(b2);
	panel.add(nameField);
	panel.add(activityTypeList);

	b1.addActionListener(e-> { 
		File actFile = filechooser();
		filePath = actFile.getAbsolutePath();
	});
	

	b2.addActionListener(e-> { 
		cont.createActivity(cont.activityCount() +1 , nameField.getText(), activityTypeList.getSelectedItem().toString());
		cont.updateData(filePath, cont.activityCount());
	});
	
	return activityFrame;
}


public File filechooser() {
	JFileChooser filechooser = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
	int returnValue = filechooser.showOpenDialog(null);
	File selectedFile = null;
	// int returnValue = jfc.showSaveDialog(null);

	if (returnValue == JFileChooser.APPROVE_OPTION) {
		selectedFile = filechooser.getSelectedFile();
		System.out.println(selectedFile.getAbsolutePath());
	}

	
	return selectedFile;
	
}

public JPanel drawPoints() {
	
	
	
	
	return null;
}


public static void main (String [] args) {
	
	SwingUtilities.invokeLater(() -> new ActivityClockGUI());
	
}
	
	




}
