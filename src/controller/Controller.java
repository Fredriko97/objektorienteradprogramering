package controller;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import activity.Activity;
import data.DataRead;
import database.ActivityDAO;
import database.DbConnectionManager;
import database.PointDAO;
import database.UserDAO;
import database.UserInfoDAO;
import presentation.DataPresentation;
import trackPoint.TrackPoint;
import user.User;
import view.ActivityClockGUI;

public class Controller {


	
	ActivityDAO activityDao = new ActivityDAO();
	
	PointDAO pointDao = new PointDAO();
	
	UserDAO userDao = new UserDAO();
	
	UserInfoDAO userInfoDao = new UserInfoDAO();
	
	
	private int currentUser;
	private ArrayList<Activity> activityList;
	
	public Controller () {
		
	}
	
	
	
	public boolean login(String name, String pw) throws SQLException {
		
		UserDAO userDao = new UserDAO();
		
		boolean correct = false;
		String password = "";
		String userName = "";
		
		
		while(!correct) {
			password = userDao.getPW(name);
			
			if(password.equals(pw)) {
				correct = true;
				userName = name;
			}
		}
		
		currentUser = userDao.getId(name);
		System.out.println("Inloggad som user id:" + currentUser);
		
		return correct;		
	}
	
	public ArrayList<Activity> getActivity() {
		this.activityList = (ArrayList<Activity>) activityDao.getAll(currentUser);
		return this.activityList;
	}
	
	public void updateData (String path, int activityId) {
		DataRead data = new DataRead(path);
		pointDao.update(data.readFile(), activityId);  
	}

	public String [] getUserInfo(int userId) {
		
		User user = userDao.get(userId);
		
	String [] returnInfo = new String[3];
	
	returnInfo[0] = Integer.toString(user.getUserID());
	returnInfo[1] = user.getUserName();
	returnInfo[2] = user.getUserPW();
	
		return returnInfo;
	}
	
	public void createActivity(int id, String name, String typ) {
		int typeId = 0;
		if (typ.equals("L�pning")) {
			typeId = 1;
		}
		else if (typ.equals("Cyckling")) {
			typeId = 2;
		}
		else if (typ.equals("Simmning")) {
			typeId = 3;
		}
		else if (typ.equals("�vrigt")) {
			typeId = 4;
		}
		Activity activity = new Activity(id, name, this.currentUser, typeId);
		activityDao.update(activity);
	}
	
	public int getUserId(String userName) {
		return userDao.getId(userName);
	}
	
	public String[] getStats(int activityId) {
		Activity activity = activityDao.get(activityId);
		DataPresentation dataPresentation = new DataPresentation(activity);
		
		activity.setPointList();
		String[] returnArray = new String[19];
		returnArray[0]=dataPresentation.startTime(activity);
		returnArray[1]=dataPresentation.endTime(activity);
		returnArray[2]=String.format("%fh %fm %fs", dataPresentation.elapsedTime(activity)/3600,(dataPresentation.elapsedTime(activity)%3600)/60,dataPresentation.elapsedTime(activity)%60);
		returnArray[3]=String.format("%.1f m",dataPresentation.getTotalDistance(activity));
		returnArray[4]=String.format("%.2f BPM",dataPresentation.getMinHeartRate(activity));
		returnArray[5]=String.format("%.2f BPM",dataPresentation.getMaxHeartRate(activity));
		returnArray[6]=String.format("%.2f BPM",dataPresentation.getAvargeHeartRate(activity));
		returnArray[7]=String.format("%.2f km/h",dataPresentation.getMinSpeed(activity));
		returnArray[8]=String.format("%.2f km/h",dataPresentation.getMaxSpeed(activity));
		returnArray[9]=String.format("%.2f km/h",dataPresentation.getAverageSpeed(activity));
		returnArray[10]=String.format("%.1f m",dataPresentation.getMinAltitude(activity));
		returnArray[11]=String.format("%.1f m",dataPresentation.getMaxAltitude(activity));
		returnArray[12]=String.format("%.1f m",dataPresentation.getAverageAltitude(activity));
		returnArray[13]=String.format("%.2f \u00B0",dataPresentation.getMinInclination(activity));
		returnArray[14]=String.format("%.2f \u00B0",dataPresentation.getMaxInclination(activity));
		returnArray[15]=String.format("%.2f \u00B0",dataPresentation.getAverageInclination(activity));
		returnArray[16]=String.format("%.1f st/min",dataPresentation.getMinCadence(activity));
		returnArray[17]=String.format("%.1f st/min",dataPresentation.getMaxCadence(activity));
		returnArray[18]=String.format("%.1f st/min",dataPresentation.getAverageCadence(activity));

		return returnArray;
	}
	
	
	public List<List> getGraphData(int activityId, int dataType){
		Activity activity = activityDao.get(activityId);
		activity.setPointList();
		List<List> list = new ArrayList<List>();
		List<Double> data = new ArrayList<Double>();
		List<Double> time = new ArrayList<Double>();
		for(TrackPoint p:activity.getPointList()) {
			data.add(p.getHeartRate());
			time.add(p.getElapsedTime());
		}
		list.add(data);
		list.add(time);
		return list;
	}
	
	public List <List> getSpeedData(int activityId, int dataType){
		
		Activity activity = activityDao.get(activityId);
		activity.setPointList();
		
		List<List> list = new ArrayList<List>();
		List<Double> data = new ArrayList<Double>();
		List<Double> time = new ArrayList<Double>();
		for(TrackPoint p:activity.getPointList()) {
			data.add(p.getSpeed());
			time.add(p.getElapsedTime());
		}
		list.add(data);
		list.add(time);
		return list;
	}
public List <List> getAltitudeData(int activityId, int dataType){
		
		Activity activity = activityDao.get(activityId);
		activity.setPointList();
		
		List<List> list = new ArrayList<List>();
		List<Double> data = new ArrayList<Double>();
		List<Double> time = new ArrayList<Double>();
		for(TrackPoint p:activity.getPointList()) {
			data.add(p.getAltitude());
			time.add(p.getElapsedTime());
		}
		list.add(data);
		list.add(time);
		return list;
	}
	
	public int activityCount() {
		
		ArrayList<Activity> activityList = activityDao.getAll();
		
		int count = activityList.size();
		
		return (count); 
	}
	
	
	public void updateName(String name, int activityId) {
		activityDao.updateName(activityId, name);
	}
	

	public ArrayList getUserInfo2() {
		ArrayList list = userInfoDao.get(currentUser);
		return list;
	}
	
	public void updateUserInfo(int age, int weight) {
		userInfoDao.update(currentUser, age, weight);
	}
}
