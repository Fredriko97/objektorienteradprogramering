package database;
import java.util.List;
import trackPoint.TrackPoint;

public interface DaoType<T> {

	List<T> getAll();
	T get(int id);
	T save(T t);
	T update(T t, String[] params);
	T delete(T t);
}
