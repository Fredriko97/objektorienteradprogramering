package database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import user.User;

public class UserInfoDAO {
	
	private DbConnectionManager dbConManagerSingleton = null;
	
	public UserInfoDAO() {
		dbConManagerSingleton =DbConnectionManager.getInstance();
	}
	
	
	
	public void update(int userId, int age, int weight) {
		
		PreparedStatement preparedStatement = null; 
		
		
		try {
			
			preparedStatement = this.dbConManagerSingleton.prepareStatement("update user_info set user_age = ?, user_weight = ? where user_id = ?");
			
			preparedStatement.setInt(1, age);
			preparedStatement.setInt(2, weight);
			preparedStatement.setInt(3, userId);
			
			
			preparedStatement.execute();
			
			this.dbConManagerSingleton.close();
		}
		 catch (SQLException e) {
			e.printStackTrace();
		}	
	}
	
public ArrayList get(int id) {
		
		ArrayList userInfo = new ArrayList();
		
		try {
			
			ResultSet resultSet = this.dbConManagerSingleton.excecuteQuery("SELECT user_age, user_weight FROM user_info WHERE user_id =" + id);
				resultSet.next();
				userInfo.add(resultSet.getInt(1));
				userInfo.add(resultSet.getInt(2));
				
			this.dbConManagerSingleton.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return userInfo;
	}
	
}
