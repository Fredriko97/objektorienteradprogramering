package database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.postgresql.util.PSQLException;

import activity.Activity;
import user.User;

public class UserDAO {
	
	private DbConnectionManager dbConManagerSingleton = null;
	
	
	public UserDAO() {
		dbConManagerSingleton =DbConnectionManager.getInstance();
	}
	
	public UserDAO(int user_id, String user_name, String user_pw) {
		// SKAPA USER
	}

	public void update(int id, String name, String pw) {
		
		PreparedStatement preparedStatement = null; 
		
		
		try {
			
			preparedStatement = this.dbConManagerSingleton.prepareStatement("Insert INTO user_credentials(user_id, user_name, user_pw) values(?, ?, ?)");
			
			preparedStatement.setInt(1, id);
			preparedStatement.setString(2, name);
			preparedStatement.setString(3, pw);
			
			
			preparedStatement.execute();
			
			this.dbConManagerSingleton.close();
		}
		 catch (SQLException e) {
			e.printStackTrace();
		}	
	}

	public List<User> getAll() {
		List<User> userList = new ArrayList<User>();
		
		
		try {
			ResultSet resultSet = this.dbConManagerSingleton.excecuteQuery("SELECT user_id, user_pw, user_name FROM user_credentials");
			while(resultSet.next()) {
				User user = new User();
				user.setUserID(resultSet.getInt(1));
				user.setUserPW(resultSet.getString(2));
				user.setUserName(resultSet.getString(3));
			}
			this.dbConManagerSingleton.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return userList;
	}

	public int getId(String name) {
		
		int id = 0;
		
		try {
			ResultSet resultSet = this.dbConManagerSingleton.excecuteQuery("SELECT user_id FROM user_credentials WHERE user_name =" + "'" + name +"'");
			resultSet.next();
			id = resultSet.getInt(1);
			dbConManagerSingleton.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return id;
		
	}

	public User get(int id) {
		
		User user = new User();
		
		try {
			
			ResultSet resultSet = this.dbConManagerSingleton.excecuteQuery("SELECT user_id, user_pw, user_name FROM user_credentials WHERE user_id =" + id);
				//user = new User(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3));
				resultSet.next();
				user.setUserID(resultSet.getInt(1));
				user.setUserPW(resultSet.getString(2));
				user.setUserName(resultSet.getString(3));
				
			this.dbConManagerSingleton.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return user;
	}
	
	public String getPW(String user) {
		
		String pw = null;
		
		
		try {	
			ResultSet resultSet = this.dbConManagerSingleton.excecuteQuery("SELECT user_pw FROM user_credentials "
					+ "WHERE user_name =" + "'" +user+"'");
			if(!resultSet.next()) {
				throw new NoSuchElementException(" No such element");
			
			}
			else {
				pw = resultSet.getString(1);
				this.dbConManagerSingleton.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return pw;
	}
	
	public void delete(int id) {
		
		PreparedStatement preparedStatement = null; 
		try {
		preparedStatement = this.dbConManagerSingleton.prepareStatement("DELETE FROM user_credentials WHERE user_id = ? values(?)");
		
		preparedStatement.setInt(id, 1);
		
		this.dbConManagerSingleton.close();
		
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	
	
	/*public int addUser() {
		
		
	}*/

}
