package database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import activity.Activity;
import trackPoint.TrackPoint;

public class ActivityDAO {

	DbConnectionManager dbConManagerSingleton = null;
	
	public ActivityDAO() {
		dbConManagerSingleton =DbConnectionManager.getInstance();
	}
	

	public void update(Activity activity) {

		PreparedStatement preparedStatement = null;
		dbConManagerSingleton = DbConnectionManager.getInstance();

		try {

			preparedStatement = dbConManagerSingleton
					.prepareStatement("Insert INTO activity(activity_id, activity_name, type_id, user_id) values(?, ?, ?, ?)");

			preparedStatement.setInt(1, activity.getActivityId());
			preparedStatement.setString(2, activity.getName());
			preparedStatement.setInt(3, activity.getTypeId());
			preparedStatement.setInt(4, activity.getUserId());

			preparedStatement.execute();

			dbConManagerSingleton.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	

	public ArrayList<Activity> getAll() {

		dbConManagerSingleton = DbConnectionManager.getInstance();
		ArrayList<Activity> activityList = new ArrayList<Activity>();

		try {
			ResultSet resultSet = dbConManagerSingleton
					.excecuteQuery("SELECT activity_id, activity_name, user_id, type_id FROM activity");
			while (resultSet.next()) {
				activityList.add(new Activity(resultSet.getInt(1), resultSet.getString(2).trim(), resultSet.getInt(3), resultSet.getInt(4)));
			}
			dbConManagerSingleton.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return activityList;
	}

	public ArrayList<Activity> getAll(int userId) {

		dbConManagerSingleton = DbConnectionManager.getInstance();
		List<Activity> activityList = new ArrayList<Activity>();

		try {
			ResultSet resultSet = dbConManagerSingleton
					.excecuteQuery("SELECT activity_id, activity_name, user_id, type_id FROM activity where user_id =" + userId);
			while (resultSet.next()) {
				activityList.add(new Activity(resultSet.getInt(1), resultSet.getString(2).trim(), resultSet.getInt(3), resultSet.getInt(4)));
			}
			dbConManagerSingleton.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return (ArrayList<Activity>) activityList;
	}

	public Activity get(int activityId) {
		Activity activity = null;
		try {
			ResultSet resultSet = dbConManagerSingleton.excecuteQuery(
					"SELECT activity_id, activity_name, user_id, type_id FROM activity WHERE activity_id=" + activityId);
			if (!resultSet.next())
				throw new NoSuchElementException("The activity with id " + activityId + " doesen't exist in database");
			else
				activity = new Activity(activityId, resultSet.getString(2), resultSet.getInt(3), resultSet.getInt(4));
			dbConManagerSingleton.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return activity;

	}
	
	public void updateName(int activityId, String name) {
		
		PreparedStatement preparedStatement = null;

		try {

			preparedStatement = dbConManagerSingleton
					.prepareStatement("update activity set activity_name = " + "'" + name + "'" + " where activity_id = " + activityId );

			preparedStatement.execute();

			dbConManagerSingleton.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
