package user;

import database.DbConnectionManager;
import database.UserDAO;

public class UserManager {

	
	public User getUser(int userId) {
		
		UserDAO userDao = new UserDAO();
		User user = userDao.get(userId);
		
		return user;
	}
	
	public void createUser(String name, String pw, int id) {
		User user = new User();
		user.setUserID(id);
		user.setUserName(name);
		user.setUserPW(pw);
		
		UserDAO userDao = new UserDAO();
		userDao.update(id, name, pw);
		
	}
	

}
