package user;

import java.util.List;

import activity.Activity;
import database.ActivityDAO;

public class User {

	private String userName;
	private String userPW;
	private int userID;
	private List<Activity> userActivitys;
	
	
	public void setActivityList() {
		ActivityDAO activityDao = new ActivityDAO();
		userActivitys = activityDao.getAll(this.userID);
	}
	
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserPW() {
		return userPW;
	}
	public void setUserPW(String userPW) {
		this.userPW = userPW;
	}
	public int getUserID() {
		return userID;
	}
	public void setUserID(int userID) {
		this.userID = userID;
	}
	
	

}
