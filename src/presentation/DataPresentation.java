package presentation;

import java.util.ArrayList;
import java.util.List;

import activity.Activity;
import trackPoint.TrackPoint;


public class DataPresentation {

	private Activity activity;
	
	public DataPresentation(Activity activity) {
		this.activity = activity;
	}
	
	public double getTotalDistance(Activity activity) { // TOTALDISTANS
		double sumTD = 0;
		for (int i = 0; i < this.activity.getPointList().size(); i++) {
			sumTD = this.activity.getPointList().get(i).getDistance();
		}
		return sumTD;
	}

	public double getAvargeHeartRate(Activity activity) { // MEDELPULS
		double sumHR = 0;
		for (int i = 0; i < this.activity.getPointList().size(); i++) {
			sumHR = sumHR + this.activity.getPointList().get(i).getHeartRate();
		}
		double avgHR = sumHR / this.activity.getPointList().size();
		return avgHR;
	}

	public double getMaxHeartRate(Activity activity) { // MAXPULS
		double maxHR = 0;
		for (int i = 0; i < this.activity.getPointList().size(); i++) {
			if (maxHR < this.activity.getPointList().get(i).getHeartRate())
				maxHR = this.activity.getPointList().get(i).getHeartRate();
		}
		return maxHR;
	}
	
	public double getMinHeartRate(Activity activity) { //MINPULS
		double minHR = this.activity.getPointList().get(0).getHeartRate(); 
		for (int i = 0; i < this.activity.getPointList().size(); i++) {
			if(minHR > this.activity.getPointList().get(i).getHeartRate())
				minHR = this.activity.getPointList().get(i).getHeartRate();
	}
		return minHR;
	}

	public double getAverageAltitude(Activity activity) { // MEDELALTITUD

		double sumA = 0;
		for (int i = 0; i < this.activity.getPointList().size(); i++) {
			sumA = sumA + this.activity.getPointList().get(i).getAltitude();
		}

		double avgA = sumA / this.activity.getPointList().size();
		return avgA;
	}

	public double getMaxAltitude(Activity activity) {  // MAXALTITUD
		double maxA = 0;
		for (int i = 0; i < this.activity.getPointList().size(); i++) {
			if (maxA < this.activity.getPointList().get(i).getAltitude())
				maxA = this.activity.getPointList().get(i).getAltitude();
		}
		return maxA;

	}
	
	public double getMinAltitude(Activity activity) { //MINALTITUDE
		double minA = this.activity.getPointList().get(0).getAltitude(); 
		for (int i = 0; i < this.activity.getPointList().size(); i++) {
			if(minA > this.activity.getPointList().get(i).getAltitude())
				minA = this.activity.getPointList().get(i).getAltitude();
	}
		return minA;
	}
	
	

	public double getAverageSpeed(Activity activity) { //MEDELHASTIGHET 
		double sumAS = 0;
		for (int i = 0; i < this.activity.getPointList().size(); i++) {
			if(i != 0) {
			sumAS = sumAS + this.activity.getPointList().get(i).getSpeed();
			}
		}
		double avgAS = sumAS / this.activity.getPointList().size();
		return avgAS;	
	}

	public double getMaxSpeed(Activity activity) {   //MAXHASTIGHET
		double maxMS = 0;
		for (int i = 0; i < this.activity.getPointList().size(); i++) {
			if (maxMS < this.activity.getPointList().get(i).getSpeed())
				maxMS = this.activity.getPointList().get(i).getSpeed();
		}
		return maxMS;
	}
	
	public double getMinSpeed(Activity activity) { //MINHASTIGHET (inte klar, sortera bort nollvärden ifrån tabellen)
		
	
		double minS = this.activity.getPointList().get(0).getSpeed(); 
		for (int i = 0; i < this.activity.getPointList().size(); i++) {
			if(minS > this.activity.getPointList().get(i).getSpeed())
				minS = this.activity.getPointList().get(i).getSpeed();
	}
		return minS;
	}
	
	
	

	public double getAverageCadence(Activity activity) { //MEDELKADENS 
		double sumAC = 0;
		for (int i = 0; i < this.activity.getPointList().size(); i++) {
			if(i != 0) {
			sumAC = sumAC + this.activity.getPointList().get(i).getCadence();
			}
		}
		double avgAC = sumAC / this.activity.getPointList().size();
		return avgAC;	

	}

	public double getMaxCadence(Activity activity) {   //MAXKADENS
		double maxC = 0;
		for (int i = 0; i < this.activity.getPointList().size(); i++) {
			if (maxC < this.activity.getPointList().get(i).getCadence())
				maxC = this.activity.getPointList().get(i).getCadence();
		}
		return maxC;
	}
	
	
	public double getMinCadence(Activity activity) { //MINKADENS  (INTE KLAR, SORTERA BORT NOLLVÄRDEN)
		double minC = 0; 
		
		for (int i = 0; i < this.activity.getPointList().size(); i++) {
			
			if(minC > this.activity.getPointList().get(i).getCadence())
				minC = this.activity.getPointList().get(i).getCadence();
	}
		return minC;
	}
	
	public double getAverageInclination(Activity activity) { // MEDELLUTNING (INTE KLAR)
		double heightDiff;
		double lengthDiff;
		double totalIncline = 0;

		for (int i = 0; i < this.activity.getPointList().size() - 1; i++) {
			heightDiff = this.activity.getPointList().get(i).getAltitude()
					- this.activity.getPointList().get(i + 1).getAltitude();
			lengthDiff = this.activity.getPointList().get(i).getDistance()
					- this.activity.getPointList().get(i + 1).getDistance();
			totalIncline += (double) (Math.atan(heightDiff / lengthDiff) * (180 / Math.PI));
		}
		return totalIncline / this.activity.getPointList().size();
	}

	public double getMaxInclination(Activity activity) { // MAXLUTNING 
		double heightDiff = Math
				.abs(this.activity.getPointList().get(0).getAltitude() - this.activity.getPointList().get(1).getAltitude());
		double lengthDiff = Math
				.abs(this.activity.getPointList().get(0).getDistance() - this.activity.getPointList().get(1).getDistance());
		double maxInc = (double) (Math.atan(heightDiff / lengthDiff) * (180 / Math.PI));
		for (int i = 1; i < this.activity.getPointList().size() - 1; i++) {
			heightDiff = Math.abs(
					this.activity.getPointList().get(i).getAltitude() - this.activity.getPointList().get(i + 1).getAltitude());
			lengthDiff = Math.abs(
					this.activity.getPointList().get(i).getDistance() - this.activity.getPointList().get(i + 1).getDistance());
			if ((float) (Math.atan(heightDiff / lengthDiff) * (Math.PI / 180)) > maxInc) {
				maxInc = (float) (Math.atan(heightDiff / lengthDiff) * (180 / Math.PI));
			}
		}
		return maxInc;
	}

	public double getMinInclination(Activity activity) { // MINSTALUTNING 

		double heightDiff = Math.abs(this.activity.getPointList().get(0).getAltitude()-this.activity.getPointList().get(1).getAltitude());
		double lenghtDiff = Math.abs(this.activity.getPointList().get(0).getDistance()-this.activity.getPointList().get(1).getDistance());
		double minInc=(float) (Math.atan(heightDiff/lenghtDiff)*(180/Math.PI));
		for(int i=1;i<this.activity.getPointList().size()-1;i++) {
			heightDiff = Math.abs(this.activity.getPointList().get(i).getAltitude()-this.activity.getPointList().get(i+1).getAltitude());
			lenghtDiff = Math.abs(this.activity.getPointList().get(i).getDistance()-this.activity.getPointList().get(i+1).getDistance());
			if((double)(Math.atan(heightDiff/lenghtDiff)*(180/Math.PI))<minInc) {
				minInc=(double)(Math.atan(heightDiff/lenghtDiff)*(180/Math.PI));
			}
			
		}
		return minInc;

		
	
	}
	
	public String startTime(Activity activity) {
		return activity.getPointList().get(0).getTime();
	}
	public String endTime(Activity activity) {
		return activity.getPointList().get(activity.getPointList().size()-1).getTime();	
	}
	public double elapsedTime(Activity activity) {
		return activity.getPointList().get(activity.getPointList().size()-1).getElapsedTime();
		
	}
	
	public ArrayList mapPoints(Activity activity) {
		
		ArrayList xList = new ArrayList();
		ArrayList yList = new ArrayList();
		
		List<TrackPoint> pointList = activity.getPointList();
		
		for(int i = 0; i < pointList.size(); i++) {
			xList.add(pointList.get(i).getLatitude());
		}
		
		for(int i = 0; i < pointList.size(); i++) {
			yList.add(pointList.get(i).getLongitude());
		}
		
		return null;
	}


}



	


